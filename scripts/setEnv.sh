export SPRING_DATASOURCE_URL=jdbc:postgresql://localhost:5432/postgres
export SPRING_DATASOURCE_USERNAME=postgres
export SPRING_DATASOURCE_PASSWORD=postgres

#export REACT_APP_URL=http://`ipconfig getifaddr en0`:8080
export REACT_APP_URL=https://localhost:8443

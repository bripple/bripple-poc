package io.bripple.poc.repository;

import java.time.LocalDateTime;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import io.bripple.poc.model.PocCase;
import org.junit.Assert;
import lombok.extern.slf4j.Slf4j;

@RunWith(SpringRunner.class)
@DataJpaTest
@Slf4j
public class PocCaseRepositoryTest {
	
	@Autowired
	private PocCaseRepository pocCaseRepository;
	
	@Test
	public void impactPointTest() throws InterruptedException {

		long count = pocCaseRepository.count();
		
		Assert.assertEquals(0, count);
		
		PocCase pocCase1 = PocCase.builder()
				.deedId(1001)
				.userName("Arsene")
				.updateTime(LocalDateTime.now())
				.build();
		
		PocCase pocCase2 = PocCase.builder()
				.deedId(1001)
				.userName("Cara")
				.updateTime(LocalDateTime.now())
				.build();
		
		PocCase pocCase3 = PocCase.builder()
				.deedId(1001)
				.userName("Luka")
				.updateTime(LocalDateTime.now())
				.build();
		
		PocCase pocCase4 = PocCase.builder()
				.deedId(1001)
				.userName("Arsene")
				.updateTime(LocalDateTime.now())
				.build();
		
		PocCase pocCase5 = PocCase.builder()
				.deedId(1002)
				.userName("Arsene")
				.updateTime(LocalDateTime.now())
				.build();

		PocCase save1 = pocCaseRepository.save(pocCase1);
		Thread.sleep(1000);
		pocCaseRepository.save(pocCase2);
		Thread.sleep(1000);
		pocCaseRepository.save(pocCase3);
		Thread.sleep(1000);
		PocCase save4 = pocCaseRepository.save(pocCase4);
		Thread.sleep(1000);
		pocCaseRepository.save(pocCase5);
		
		log.debug("save1 time: " + save1.getUpdateTime());
		log.debug("save4 time: " + save4.getUpdateTime());
		
		List<PocCase> findByUserNameOrderByUpdateTimeAsc = pocCaseRepository.findByDeedIdAndUserNameOrderByUpdateTimeAsc(1001, "Arsene");
		PocCase pocCase = findByUserNameOrderByUpdateTimeAsc.get(0);
		
		Assert.assertEquals(save1.getUpdateTime(), pocCase.getUpdateTime());
		
		List<PocCase> findByUserNameAndUpdateTimeGreaterThan = pocCaseRepository.findByDeedIdAndUpdateTimeGreaterThanEqual(save1.getDeedId(), save1.getUpdateTime());
		
		log.info("size: " + findByUserNameAndUpdateTimeGreaterThan.size());
		Assert.assertEquals(4, findByUserNameAndUpdateTimeGreaterThan.size());
	}
}

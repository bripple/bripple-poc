/* eslint-disable react/jsx-filename-extension */
/* eslint-env browser */
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';
import createHistory from 'history/createBrowserHistory';
import { App } from 'containers/App';
import configureStore from './configureStore';

import 'semantic-ui-css/semantic.min.css';
import './index.css';

const initialState = {};
const history = createHistory();
const store = configureStore(initialState, history);

if (module.hot) {
  module.hot.accept();
}

ReactDOM.render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <App />
    </ConnectedRouter>
  </Provider>,
  document.getElementById('react'),
);

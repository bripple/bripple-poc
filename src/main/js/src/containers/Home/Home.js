import React, { Component } from 'react';
import { Feed, Icon } from 'semantic-ui-react';
import TimeAgo from 'react-timeago';
import { getImpactNumber } from 'utils/APIUtils';
import Alert from 'react-s-alert';
import FlexView from 'react-flexview';

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      impactNumber: 0,
      deeds: [],
    };
  }

  componentDidMount() {
    getImpactNumber()
      .then(response => {
        console.log(response);
        this.setState({
          impactNumber: response.length,
          deeds: response,
        });
      })
      .catch(error => {
        Alert.error(
          (error && error.message) ||
            'Oops! Something went wrong getting impact number. Please try again!',
        );
      });
  }

  render() {
    // FIXME: Switch to props
    const { impactNumber } = this.state;
    const events = this.state.deeds.map((deed, i) => ({
      summary: (
        <div>
          <strong>{deed.userName}</strong> forwarded deed[{deed.deedId}] from{' '}
          <strong>{deed.deedCreator}</strong> <TimeAgo date={deed.updateTime} />
        </div>
      ),
      extraImages: [<Icon name="angle double down" />],
    }));
    return (
      <FlexView>
        <div style={{ marginTop: '80px', marginLeft: '20%' }}>
          <h1>Hi, {this.props.currentUser}</h1>
          <h2>Here is your Impact Number: {impactNumber}</h2>
          <Feed events={events} />
        </div>
      </FlexView>
    );
  }
}

export default Home;

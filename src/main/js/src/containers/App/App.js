import React, { Component } from 'react';
import Alert from 'react-s-alert';
import { Switch, Route } from 'react-router-dom';
import { Header } from 'components/Header';
import { Login } from 'containers/Login';
import { Profile } from 'containers/Profile';
import { Signup } from 'containers/Signup';
import { OAuth2RedirectHandler } from 'components/OAuth2RedirectHandler';
import { getCurrentUser } from 'utils/APIUtils';
// FIXME: move constants to utils
import { ACCESS_TOKEN } from 'utils/constants';
import PrivateRoute from 'components/PrivateRoute';
import LoadingIndicator from 'components/LoadingIndicator';
import { Home } from 'containers/Home';
import { Deed } from 'containers/Deed';
import GlobalStyle from 'global-styles';

import 'react-s-alert/dist/s-alert-default.css';
import 'react-s-alert/dist/s-alert-css-effects/scale.css';

// TODO: Include redux/saga
class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      authenticated: false,
      currentUser: null,
      loading: false,
    };

    this.loadCurrentlyLoggedInUser = this.loadCurrentlyLoggedInUser.bind(this);
    this.handleLogout = this.handleLogout.bind(this);
  }

  loadCurrentlyLoggedInUser() {
    this.setState({
      loading: true,
    });

    getCurrentUser()
      .then(response => {
        console.log('currentUser', response);
        this.setState({
          currentUser: response,
          authenticated: true,
          loading: false,
        });
      })
      .catch(error => {
        this.setState({
          loading: false,
        });
      });
  }

  handleLogout() {
    localStorage.removeItem(ACCESS_TOKEN);
    this.setState({
      authenticated: false,
      currentUser: null,
    });
    Alert.success("You're safely logged out!");
  }

  componentDidMount() {
    this.loadCurrentlyLoggedInUser();
  }

  render() {
    if (this.state.loading) {
      return <LoadingIndicator />;
    }
    return (
      <div>
        <Header
          authenticated={this.state.authenticated}
          onLogout={this.handleLogout}
        />
        <Switch>
          <Route
            exact
            path="/"
            component={Home}
            currentUser={this.state.currentUser}
          />
          <Route path="/deed" component={Deed} />
          <Route path="/oauth2/redirect" component={OAuth2RedirectHandler} />

          <PrivateRoute
            path="/profile"
            authenticated={this.state.authenticated}
            currentUser={this.state.currentUser}
            component={Profile}
          />
          <Route
            path="/login"
            render={props => (
              <Login authenticated={this.state.authenticated} {...props} />
            )}
          />
          <Route
            path="/signup"
            render={props => (
              <Signup authenticated={this.state.authenticated} {...props} />
            )}
          />

          <PrivateRoute
            path="/profile"
            authenticated={this.state.authenticated}
            currentUser={this.state.currentUser}
            component={Profile}
          />
        </Switch>
        <Alert
          stack={{ limit: 1 }}
          position="bottom"
          effect="scale"
          timeout={3000}
        />
        <GlobalStyle />
      </div>
    );
  }
}

export default App;

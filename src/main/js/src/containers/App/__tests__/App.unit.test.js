import React from 'react';
import { shallow } from 'enzyme';
import App from '../App';
import { Header } from '../../Header';
import { Switch } from 'react-router-dom';

describe('Test App', () => {
  let wrapper;
  
  beforeEach(()=>{
    wrapper = shallow(<App />);
    console.log(<App/>);
  });

  test('App home page', async() => {
    expect(wrapper.find(Header).length).toEqual(1);
    expect(wrapper.find(Switch).length).toEqual(1);
  });
});
import React, { Component } from 'react';
import './Login.css';
import { Link, Redirect } from 'react-router-dom';
import Alert from 'react-s-alert';
import {
  GOOGLE_AUTH_URL,
  FACEBOOK_AUTH_URL,
  GITHUB_AUTH_URL,
} from 'utils/constants';
import fbLogo from 'components/img/fb-logo.png';
import googleLogo from 'components/img/google-logo.png';
import githubLogo from 'components/img/github-logo.png';

import LoginForm from './LoginForm';

class Login extends Component {
  componentDidMount() {
    if (this.props.location.state && this.props.location.state.error) {
      setTimeout(() => {
        Alert.error(this.props.location.state.error, {
          timeout: 5000,
        });
        this.props.history.replace({
          pathname: this.props.location.pathname,
          state: {},
        });
      }, 100);
    }
  }

  render() {
    if (this.props.authenticated) {
      return (
        <Redirect
          to={{
            pathname: '/',
            state: { from: this.props.location },
          }}
        />
      );
    }

    return (
      <div className="login-container">
        <div className="login-content">
          <h1 className="login-title">Login to Bripple</h1>
          <SocialLogin />
          <div className="or-separator">
            <span className="or-text">OR</span>
          </div>
          <LoginForm {...this.props} />
          <span className="signup-link">
            New user?
            <Link to="/signup">Sign up!</Link>
          </span>
        </div>
      </div>
    );
  }
}

const SocialLogin = () => (
  <div className="social-login">
    <a className="btn btn-block social-btn google" href={GOOGLE_AUTH_URL}>
      <img src={googleLogo} alt="Google" />
      Log in with Google
    </a>
    <a className="btn btn-block social-btn facebook" href={FACEBOOK_AUTH_URL}>
      <img src={fbLogo} alt="Facebook" />
      Log in with Facebook
    </a>
    <a className="btn btn-block social-btn github" href={GITHUB_AUTH_URL}>
      <img src={githubLogo} alt="Github" />
      Log in with Github
    </a>
  </div>
);

export default Login;

// import Alert from 'react-s-alert';
import { call, put, takeLatest } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import { http } from 'utils/API';
import { USER_LOGIN_REQUEST } from './constants';
import { userLoginError, userLoginSuccess } from './actions';

export function* userLogin(action) {
  try {
    const { email, password } = action.payload;
    const loginRequest = {
      email,
      password,
    };
    const response = yield call(http.post, '/auth/login', loginRequest);
    const reponseData = response.data;
    yield put(userLoginSuccess(reponseData.accessToken));
    yield put(push('/'));
  } catch (error) {
    yield put(userLoginError(error));
  }
}

export default function* loginFlow() {
  yield takeLatest(USER_LOGIN_REQUEST, userLogin);
}

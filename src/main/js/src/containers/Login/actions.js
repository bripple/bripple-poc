import {
  USER_LOGIN_REQUEST,
  USER_LOGIN_SUCCESS,
  USER_LOGIN_ERROR,
} from './constants';

export const userLoginRequest = (email, password) => ({
  type: USER_LOGIN_REQUEST,
  payload: {
    email,
    password,
  },
});

export const userLoginSuccess = accessToken => ({
  type: USER_LOGIN_SUCCESS,
  payload: accessToken,
});

export const userLoginError = error => ({
  type: USER_LOGIN_ERROR,
  payload: error,
});

export const USER_LOGIN_REQUEST = 'bripple/Login/USER_LOGIN_REQUEST';
export const USER_LOGIN_SUCCESS = 'bripple/Login/USER_LOGIN_SUCCESS';
export const USER_LOGIN_ERROR = 'bripple/Login/USER_LOGIN_ERROR';

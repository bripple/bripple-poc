import { ACCESS_TOKEN } from 'utils/constants';
import {
  USER_LOGIN_REQUEST,
  USER_LOGIN_SUCCESS,
  USER_LOGIN_ERROR,
} from './constants';

// TODO immer immutable
const initialState = {
  requesting: false,
  accessToken: null,
  error: null,
  currentUser: null,
};

const loginReducer = (state = initialState, action) => {
  switch (action.type) {
    case USER_LOGIN_REQUEST:
      return {
        ...state,
        requesting: true,
        accessToken: null,
        error: null,
      };
    case USER_LOGIN_SUCCESS:
      // TODO: get user name
      localStorage.setItem(ACCESS_TOKEN, action.payload);
      return {
        ...state,
        requesting: false,
        accessToken: action.payload,
      };
    case USER_LOGIN_ERROR:
      return {
        ...state,
        requesting: false,
        error: action.payload,
      };
    default:
      return state;
  }
};

export default loginReducer;

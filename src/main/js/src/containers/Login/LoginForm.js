// import Alert from 'react-s-alert';
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import Snackbar from '@material-ui/core/Snackbar';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import TextField from '@material-ui/core/TextField';
import ErrorIcon from '@material-ui/icons/Error';
import { withStyles } from '@material-ui/core/styles';

import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';
import reducer from './reducer';
import saga from './saga';
import { userLoginRequest } from './actions';

const styles = theme => ({
  error: {
    backgroundColor: theme.palette.error.dark,
  },
  message: {
    display: 'flex',
    alignItems: 'center',
  },
});

const LoginForm = props => {
  const credential = {
    email: '',
    password: '',
  };

  const handleChange = name => event => {
    credential[name] = event.target.value;
  };

  const handleSubmit = async event => {
    event.preventDefault();
    // TODO: Validate email and password
    props.userLogin(credential.email, credential.password);
  };

  const { classes, loginError } = props;
  // TODO: consolidate style with material UI and react final form
  return (
    <form onSubmit={handleSubmit}>
      {/* <RenderCounter /> */}
      <div className="form-item">
        <TextField
          label="Email"
          className="form-control"
          onChange={handleChange('email')}
        />
      </div>
      <div className="form-item">
        <TextField
          label="Password"
          type="password"
          className="form-control"
          onChange={handleChange('password')}
        />
      </div>
      <div className="form-item">
        <button type="submit" className="btn btn-block btn-primary">
          Login
        </button>
      </div>
      <div>
        <Snackbar open={loginError !== null}>
          <SnackbarContent
            className={classes.error}
            message={
              <span className={classes.message}>
                <ErrorIcon />
                You have entered an invalid username or password
              </span>
            }
          />
        </Snackbar>
      </div>
    </form>
  );
};

LoginForm.propTypes = {
  classes: PropTypes.object.isRequired,
  userLogin: PropTypes.func,
  loginError: PropTypes.object,
};

const mapStateToProps = state => {
  const { login } = state;
  return {
    loginError: login.error,
  };
};

const mapDispatchToProps = dispatch => ({
  userLogin: (email, password) => dispatch(userLoginRequest(email, password)),
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'login', reducer });
const withSaga = injectSaga({ key: 'login', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(withStyles(styles)(LoginForm));

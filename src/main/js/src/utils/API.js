import axios from 'axios';

import { REACT_APP_URL, ACCESS_TOKEN } from 'utils/constants';

const defaultOptions = {
  baseURL: REACT_APP_URL,
  method: 'get',
  headers: {
    'Content-Type': 'application/json',
  },
};

export const http = axios.create(defaultOptions);

http.interceptors.request.use(config => {
  const token = localStorage.getItem(ACCESS_TOKEN);
  const newConfig = config;
  newConfig.headers.Authorization = token ? `Bearer ${token}` : '';
  return newConfig;
});

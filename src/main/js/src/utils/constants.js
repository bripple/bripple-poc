export const RESTART_ON_REMOUNT = '@@saga-injector/restart-on-remount';
export const DAEMON = '@@saga-injector/daemon';
export const ONCE_TILL_UNMOUNT = '@@saga-injector/once-till-unmount';

export const REACT_APP_URL = process.env.REACT_APP_URL ? process.env.REACT_APP_URL : 'https://bripple-poc.herokuapp.com';
export const QR_CODE_URL = process.env.REACT_APP_QR_CODE_URL ? process.env.REACT_APP_QR_CODE_URL : REACT_APP_URL;

export const ACCESS_TOKEN = 'accessToken';

export const OAUTH2_REDIRECT_URI = REACT_APP_URL + '/oauth2/redirect';

export const GOOGLE_AUTH_URL = REACT_APP_URL + '/oauth2/authorize/google?redirect_uri=' + OAUTH2_REDIRECT_URI;
export const FACEBOOK_AUTH_URL = REACT_APP_URL + '/oauth2/authorize/facebook?redirect_uri=' + OAUTH2_REDIRECT_URI;
export const GITHUB_AUTH_URL = REACT_APP_URL + '/oauth2/authorize/github?redirect_uri=' + OAUTH2_REDIRECT_URI;

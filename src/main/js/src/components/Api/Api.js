import axios from 'axios';

import { REACT_APP_URL } from 'utils/constants';

// DEPRECATED: replaced by utils/API.js

export const Api = axios.create({
  baseURL: `${REACT_APP_URL}/api`,
});

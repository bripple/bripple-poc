import React from 'react';

import burgerLogo from '../../assets/images/bripple-logo.png';
import classes from './Logo.css';

const logo = props => (
  <div className={classes.Logo} style={{ height: props.height }}>
    <img src={burgerLogo} alt="Bripple" height="80" width="150" />
  </div>
);

export default logo;

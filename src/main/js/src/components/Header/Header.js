import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import FlexView from 'react-flexview';
import Logo from '../Logo/Logo';

class Header extends Component {
  render() {
    return (
      <header style={{ backgroundColor: 'MidnightBlue', height: '82px' }}>
        <FlexView>
          <div>
            <Logo />
          </div>
          <FlexView vAlignContent="bottom">
            <div>
              <nav>
                <ul>
                  <li style={liStyle}>
                    <Link to="/" style={linkStyle}>
                      Home
                    </Link>
                  </li>
                  <li style={liStyle}>
                    <Link to="/deed" style={linkStyle}>
                      Deed
                    </Link>
                  </li>
                  <li style={liStyle}>
                    <Link to="/login" style={linkStyle}>
                      Login
                    </Link>
                  </li>
                  <li style={liStyle}>
                    <Link to="" onClick={this.props.onLogout} style={linkStyle}>
                      Logout
                    </Link>
                  </li>
                </ul>
              </nav>
            </div>
          </FlexView>
        </FlexView>
      </header>
    );
  }
}

const liStyle = {
  display: 'inline',
  padding: '1em',
};

const linkStyle = {
  color: 'white',
};

export default Header;

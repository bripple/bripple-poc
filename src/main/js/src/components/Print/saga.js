import { put, takeLatest } from 'redux-saga/effects';
import { Api } from '../Api';
import * as actionTypes from './actionTypes';

import { addDeedIdList } from './actionCreator';

export function* createDeed(action) {
  try {
    const createDeedRequest = {
      creator: 'jhung',
      type: action.printType,
    };

    const response = yield Api.post('/deed/', createDeedRequest);

    yield put(addDeedIdList(response.data.id));
  } catch (error) {
    console.log(error);
  }
}

export default function* watchCreateDeed() {
  yield takeLatest(actionTypes.CREATE_DEED, createDeed);
}

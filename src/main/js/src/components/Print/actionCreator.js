import * as actionTypes from './actionTypes';

export const changePrintType = printType => ({
  type: actionTypes.CHANGE_PRINT_TYPE,
  val: printType,
});

export const changeNumberOfPrintouts = numberOfPrintouts => ({
  type: actionTypes.CHANGE_NUMBER_OF_PRINTOUTS,
  val: numberOfPrintouts,
});

export const changeExistingDeedId = exitingDeedId => ({
  type: actionTypes.CHANGE_EXISTING_DEED_ID,
  val: exitingDeedId,
});

export const changeDeedIdList = deedIdList => ({
  type: actionTypes.CHANGE_DEED_ID_LIST,
  val: deedIdList,
});

export const createDeed = printType => ({
  type: actionTypes.CREATE_DEED,
  printType,
});

export const addDeedIdList = deedId => ({
  type: actionTypes.ADD_DEED_ID_LIST,
  val: deedId,
});

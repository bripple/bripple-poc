import * as actionTypes from './actionTypes';

const initialState = {
  printType: 'A4',
  numberOfPrintouts: 1,
  existingDeedId: null,
  deedIdList: [],
};

const updateObject = (oldObject, updatedValues) => ({
  ...oldObject,
  ...updatedValues,
});

const reducer = (state = initialState, action) => {
  // eslint-disable-next-line default-case
  switch (action.type) {
    case actionTypes.CHANGE_PRINT_TYPE:
      return updateObject(state, { printType: action.val });
    case actionTypes.CHANGE_NUMBER_OF_PRINTOUTS:
      return updateObject(state, { numberOfPrintouts: action.val });
    case actionTypes.CHANGE_EXISTING_DEED_ID:
      return updateObject(state, { existingDeedId: action.val });
    case actionTypes.CHANGE_DEED_ID_LIST:
      return updateObject(state, { deedIdList: action.val });
    case actionTypes.ADD_DEED_ID_LIST: {
      const newDeedList = [...state.deedIdList, action.val];
      return {
        ...state,
        deedIdList: newDeedList,
      };
    }
  }
  return state;
};

export default reducer;

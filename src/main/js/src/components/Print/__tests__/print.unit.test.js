import React from 'react';
import { shallow } from 'enzyme';
import Print from '../print';

describe('Test Print feature', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<Print />);
  });

  test('Print-type dropdown should present', async () => {
    expect(wrapper.find('.print-type').length).toEqual(1);
  });

  test('Generate button should present', async () => {
    expect(wrapper.find('.generate-printout').length).toEqual(1);
  });

  // TODO: Mock axios
  test('Print type selection', () => {
    const printType = wrapper.find('.print-type');
    // Call onChange of print-type dropdown
    printType.simulate('change', null, { value: 'Cardboard' });
    const generatePrintout = wrapper.find('.generate-printout');
    generatePrintout.simulate('click');
    expect(wrapper.state('printType')).toEqual('Cardboard');
  });

  // test('Create deed', async () => {
  //   const data = await wrapper.instance().handleGenereatePrintout();
  //   console.log(data);
  // });
});

import React, { Component } from 'react';
import QRCode from 'qrcode.react';
import PropTypes from 'prop-types';
import { Grid, Icon } from 'semantic-ui-react';
import { QR_CODE_URL } from 'utils/constants';

class DeedA4 extends Component {
  render() {
    return (
      <Grid columns={2} relaxed style={{
        width: '210mm',
        maxHeight: '270mm',
        marginTop: '10mm',
        marginLeft: 'auto',
        marginRight: 'auto'
      }}>
        <div>
          <div>
            <h1>It is more blessed to give than to receive</h1>
            <h3>Bripple is a benevolence social networking platform based on Pay it forward concept.</h3>
            <p className="menu navigation-menu">
              We believe in “It is More blessed to give than to receive” philosophy,
              we want to bring happiness by providing a fun and easy to use the platform to give.  <br/><br/>
              The service will provide interaction between the givers and receivers. 
              By leveraging the ripple effect, we can see how the positive spread with time, 
              travel around the world, and eventually touch and make a impact in many people’s life.<br/><br/>
              When you saw these messages, that might mean you have received a little help from givers.
              Open the camera app of your phone and scan the QR code below to connect to Bripple platform.<br/><br/>
              You will see how far the good deed had been forwarded. We are hoping you will forward your good deed to next person.
              Let the good deed like endless ripple, continuous spreads and let more people see their impact continous happening in our sociality.
            </p>
            <h1>給予比受贈更有福</h1>
            <h3>Bripple 是個鼓勵大家用實際行動，去真正行善的平台</h3>
            <p>
                 我們相信，每個人在出生後都一直接受別人的幫助，從出生到成年得到父母的幫助，
              在學校受到同學老師的幫助，以至於到社會上我們還一直不停地得到周遭的人
              有意無意的幫助，這些令人感動的時刻，通常在事後就自然而然的被遺忘。
              我們希望藉由科技的幫助，將這個人與人的感動連結，視覺化出來並加以量化，
              讓我們看見在我們日常生活的小小舉助，幫助人一個小忙，在受助人的感動和其愛心傳遞下，
              可以達到我們想像不到的情況： 真正對你不認識的人，有改變其人生的影響。<br/><br/>

              人的一生有許多追求的目標，但是在人生的終點前，我們回頭看，
              功名成就並不能完全表達您在他人心目中的地位，最重要的還是對家人的愛與
              對社會上其他人所做的幫助和感動，我們希望這個平台可以幫助你量化你對社會的善行力量。<br/><br/>

              當你看到這個文字，代表你也許受到一個小小的幫助，用你的手機照像功能，連結到我們的平台，
              了解這個善行被傳遞多遠，也期待你將善行傳遞給下一個人，讓我們的善行像無止境的漣渏，
              一直一直傳出去，讓更多的人看見他們的善行力量在社會中持續發生。<br/><br/>
            </p>
          </div>
          <Grid>
            <Grid.Column width={3}>
              <Icon name='camera' size='massive'/>
            </Grid.Column>
            <Grid.Column width={3}>
              <Icon name='angle double right' size='massive'/>
            </Grid.Column>
            <Grid.Column width={4}>
              <QRCode value={`${QR_CODE_URL}/api/poc/${this.props.deedId}`} />
            </Grid.Column>
          </Grid>
          <p>Scan by Camera app on phone, then browser will be opened accordingly.</p>
          <p>用手機照相功能，開啟Bripple平台</p>
        </div>
      </Grid>
    );
  }
} 
export default DeedA4;

DeedA4.propTypes = {
  deedId: PropTypes.number
};
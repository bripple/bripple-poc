import React, { Component } from 'react';
import QRCode from 'qrcode.react';
import { Grid, Message, Divider } from 'semantic-ui-react';
import { REACT_APP_URL } from 'utils/constants';

class DeedCardboard extends Component {
  render() {
    return (
      <Grid columns={2} relaxed>
        <Grid.Column width={10}>
          <Message info>
            Make an impact on others lives<br/>
            Focus on what is most important in life<br/>
            Give and find out your impact point
          </Message>
        </Grid.Column>
        <Divider vertical/>
        <Grid.Column width={3}>
          <QRCode value={`${REACT_APP_URL}/api/poc/${this.props.deedId}`} />
        </Grid.Column>
      </Grid>
    )
  }
}

export default DeedCardboard;
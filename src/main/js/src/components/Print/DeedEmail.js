import React, { Component } from 'react';
import QRCode from 'qrcode.react';
import { Grid, Message, Divider } from 'semantic-ui-react';
import { REACT_APP_URL } from 'utils/constants';

class DeedEmail extends Component {
  render() {
    return (
      <div>
        <Grid columns={2} relaxed>
          <Grid.Column>
            <Message info>
              <Message.Header>It is more blessed to give than to receive</Message.Header>
              <p>Bripple is a benevolence social networking platform based on Pay it forward concept.</p>
              <p>We believe in “It is More blessed to give than to receive” philosophy, 
                we want to bring happiness by providing a fun and easy to use the platform to give. 
                The service will provide interaction between the givers and receivers. 
                By leveraging the ripple effect, we can see how the positive spread with time, 
                travel around the world, and eventually touch and make a impact in many people’s life.
              </p>
            </Message>
          </Grid.Column>
          <Divider vertical/>
          <Grid.Column>
            <QRCode value={`${REACT_APP_URL}/api/poc/${this.props.deedId}`} />
          </Grid.Column>
        </Grid>
      </div>
    )
  }
}

export default DeedEmail;

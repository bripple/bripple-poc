import React, { Component } from 'react';
import ReactToPrint from 'react-to-print';
import { Button, Divider, Form, Segment } from 'semantic-ui-react';
import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';
import { compose } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import DeedA4 from './DeedA4';
import DeedCardboard from './DeedCardboard';
import DeedEmail from './DeedEmail';
import * as actionCreators from './actionCreator';
import reducer from './reducer';
import saga from './saga';

class Print extends Component {
  static get propTypes() {
    return {
      printType: PropTypes.string,
    };
  }

  handleGenereatePrintout = event => {
    if (
      this.props.existingDeedId === null ||
      this.props.existingDeedId === ''
    ) {
      for (let i = 0; i < this.props.numberOfPrintouts; i++) {
        this.props.createDeed(this.props.printType);
      }
    } else {
      this.props.onChangeDeedIdList([this.props.existingDeedId]);
    }
  };

  checkPrintType(type) {
    return this.props.printType === type;
  }

  render() {
    const printTypes = [
      { key: 'A4', value: 'A4', text: 'A4' },
      { key: 'Cardboard', value: 'Cardboard', text: 'Cardboard' },
      { key: 'Email', value: 'Email', text: 'Email' },
    ];

    return (
      <div>
        <Segment padded>
          <Form>
            <Form.Dropdown
              className="print-type"
              placeholder="Print Type"
              search
              selection
              options={printTypes}
              onChange={(event, data) =>
                this.props.onChangePrintType(data.value)
              }
              defaultValue="A4"
            />
            <Form.Input
              label={{ content: 'Printouts' }}
              labelPosition="right"
              placeholder="Enter number of Printout"
              defaultValue="1"
              onChange={(event, data) =>
                this.props.onChangeNumberOfPrintouts(data.value)
              }
            />
            <Divider horizontal>Or</Divider>
            <Form.Input
              placeholder="Existing Deed ID (known from DB)"
              onChange={(event, data) =>
                this.props.onChangeExistingDeedId(parseInt(data.value))
              }
            />
            <Form.Button
              className="generate-printout"
              onClick={this.handleGenereatePrintout.bind(this)}
            >
              Generate
            </Form.Button>
          </Form>
        </Segment>
        <Segment padded>
          {this.checkPrintType('A4') && (
            <div>
              <ReactToPrint
                trigger={() => <Button>Print Deed on A4</Button>}
                content={() => this.deedA4}
              />
              <div ref={el => (this.deedA4 = el)}>
                {this.props.deedIdList.map((deedId, index) => (
                  <DeedA4 key={index} deedId={deedId} />
                ))}
              </div>
            </div>
          )}

          {this.checkPrintType('Cardboard') && (
            <div>
              <ReactToPrint
                trigger={() => <Button>Print Deed on Cardboard</Button>}
                content={() => this.deedCardboard}
              />
              <div ref={el => (this.deedCardboard = el)}>
                {this.props.deedIdList.map((deedId, index) => (
                  <DeedCardboard key={index} deedId={deedId} />
                ))}
              </div>
            </div>
          )}
          {this.checkPrintType('Email') && (
            <div>
              <p>Send by Email</p>
              {this.state.deedIdList.map((deedId, index) => (
                <DeedEmail key={index} deedId={deedId} />
              ))}
            </div>
          )}
        </Segment>
      </div>
    );
  }
}

const mapStateToProps = state => {
  const { print } = state;
  return {
    printType: print.printType,
    numberOfPrintouts: print.numberOfPrintouts,
    existingDeedId: print.existingDeedId,
    deedIdList: print.deedIdList,
  };
};

const mapDispatchToProps = dispatch => ({
  onChangePrintType: printType =>
    dispatch(actionCreators.changePrintType(printType)),
  onChangeNumberOfPrintouts: numberOfPrintouts =>
    dispatch(actionCreators.changeNumberOfPrintouts(numberOfPrintouts)),
  onChangeExistingDeedId: existingDeedId =>
    dispatch(actionCreators.changeExistingDeedId(existingDeedId)),
  onChangeDeedIdList: deedIdList =>
    dispatch(actionCreators.changeDeedIdList(deedIdList)),
  addDeedList: deedId => dispatch(actionCreators.addDeedIdList(deedId)),
  createDeed: printType => dispatch(actionCreators.createDeed(printType)),
});

const withReducer = injectReducer({ key: 'print', reducer });
const withSaga = injectSaga({ key: 'login', saga });

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(Print);

package io.bripple.poc.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.UpdateTimestamp;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "pocCase")
@Data
@Builder
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
public class PocCase {
	@Id
	@GeneratedValue(generator = "poccase_generator")
	@SequenceGenerator(
		name = "poccase_generator",
		sequenceName = "poccase_sequence",
		initialValue = 1
	)

	private long id;
	
	@Column
	private long deedId;
	
	@Column
	private String userName;
	
	@Column
	private String ipAddress;
	
	@Column
	private String deedCreator;
	
	@Column
	@UpdateTimestamp
	private LocalDateTime updateTime;
}

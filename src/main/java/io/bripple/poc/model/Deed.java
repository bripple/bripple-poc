package io.bripple.poc.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.UpdateTimestamp;

import lombok.Data;

@Entity
@Table(name = "deed")
@Data
public class Deed {
  @Id
  @GeneratedValue(generator = "deed_generator")
  @SequenceGenerator(
    name = "deed_generator",
    sequenceName = "deed_sequence",
    initialValue = 1
  )
  
  private long id;

  // TODO: Assoicate with user table
  @Column
  private String creator;

  @Column
  private String type;

  @Column
  @UpdateTimestamp
  private LocalDateTime createdTime;
}
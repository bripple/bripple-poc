package io.bripple.poc.model;

public enum  AuthProvider {
    local,
    facebook,
    google,
    github
}

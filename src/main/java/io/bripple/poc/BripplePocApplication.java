package io.bripple.poc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;

import io.bripple.poc.config.OAuthProperties;

@SpringBootApplication
@EntityScan(basePackageClasses = {
		BripplePocApplication.class,
		Jsr310JpaConverters.class
})
@EnableConfigurationProperties(OAuthProperties.class)
public class BripplePocApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(BripplePocApplication.class, args);
	}
	
}

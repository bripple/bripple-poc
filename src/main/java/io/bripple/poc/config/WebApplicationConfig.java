package io.bripple.poc.config;

import org.springframework.boot.web.server.ErrorPage;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.boot.web.servlet.server.ConfigurableServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/*
 * This config is to redirect 404 error when doing browser refresh with SPA architecture (single page application)
 * Reference: https://stackoverflow.com/questions/44692781/configure-spring-boot-to-redirect-404-to-a-single-page-app?noredirect=1&lq=1
 */

@Configuration
public class WebApplicationConfig implements WebMvcConfigurer {

  @Override
  public void addViewControllers(ViewControllerRegistry registry) {
    registry.addViewController("/notFound").setViewName("forward:/index.html");
    registry.addViewController("/unauthorized").setViewName("forward:/index.html");
  }

  @Bean
  public WebServerFactoryCustomizer<ConfigurableServletWebServerFactory> containerCustomizer() {
    return container -> {
        container.addErrorPages(new ErrorPage(HttpStatus.NOT_FOUND, "/notFound"));
        container.addErrorPages(new ErrorPage(HttpStatus.UNAUTHORIZED, "/unauthorized"));
    };
  }

  // FIMXE: Only allow this for development
  @Override
  public void addCorsMappings(CorsRegistry registry) {
      registry.addMapping("/api/**")
          .allowedMethods("PUT", "DELETE", "POST", "GET");
  }
}

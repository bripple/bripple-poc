package io.bripple.poc.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.bripple.poc.exception.UserNotFoundException;
import io.bripple.poc.model.Deed;
import io.bripple.poc.model.PocCase;
import io.bripple.poc.repository.DeedRepository;
import io.bripple.poc.repository.PocCaseRepository;

@Service
public class PocCaseService {
	
	@Autowired
	private PocCaseRepository pocCaseRepository;
	
	
	@Autowired
	private DeedRepository deedRepository;
	
	
	
	
	public PocCase createPocCase(PocCase pocCase) {
		
		
		Optional<Deed> deedOpt = deedRepository.findById(pocCase.getDeedId());
		
		if(deedOpt.isPresent()) {
			Deed deed = deedOpt.get();
			
			pocCase.setDeedCreator(deed.getCreator());
		}
		
		return pocCaseRepository.save(pocCase);
	}
	
	public List<PocCase> findImpactByDeedIdAndUserName(long deedId, String userName) {
		
		List<PocCase> findByUserNameOrderByUpdateTimeAsc = pocCaseRepository.findByUserNameOrderByUpdateTimeAsc(userName);
		
		if(findByUserNameOrderByUpdateTimeAsc.isEmpty()) {
			throw new UserNotFoundException("userName: " + userName + " not found in the system!");
		}
		
		PocCase pocCase = findByUserNameOrderByUpdateTimeAsc.get(0);
		
		List<PocCase> findByDeedIdAndUpdateTimeGreaterThan = pocCaseRepository.findByDeedIdAndUpdateTimeGreaterThanEqual(pocCase.getDeedId(), pocCase.getUpdateTime());
		
		return findByDeedIdAndUpdateTimeGreaterThan;
	}
	
	public List<PocCase> findImpactByUserName(String username) {
		
		List<PocCase> findByDeedCreatorOrderByUpdateTimeAsc = pocCaseRepository.findByDeedCreatorOrderByUpdateTimeAsc(username);
		
		if(findByDeedCreatorOrderByUpdateTimeAsc.isEmpty()) {
			throw new UserNotFoundException("Creator: " + username + " not found in the system!");
		}
		
//		PocCase pocCase = findByUserNameOrderByUpdateTimeAsc.get(0);
		
//		List<PocCase> findByDeedIdAndUpdateTimeGreaterThan = pocCaseRepository.findByDeedIdAndUpdateTimeGreaterThanEqual(pocCase.getDeedId(), pocCase.getUpdateTime());
		
		return findByDeedCreatorOrderByUpdateTimeAsc;
	}
}

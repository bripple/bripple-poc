package io.bripple.poc.controller;

import java.io.IOException;
import java.util.List;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.bripple.poc.model.Deed;
import io.bripple.poc.model.PocCase;
import io.bripple.poc.repository.DeedRepository;
import io.bripple.poc.repository.PocCaseRepository;
import io.bripple.poc.service.PocCaseService;

@RestController
@RequestMapping("/api")
public class PocCaseController {

	@Autowired
	private PocCaseRepository pocCaseRepository;

	@Autowired
	private PocCaseService pocCaseService;

	@Autowired
	private DeedRepository deedRepository;

	// FIXME: Create deed controller
	@PostMapping("/deed")
	public Deed createDeed(@Valid @RequestBody Deed deed, HttpServletRequest request) {
		
		
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String currentPrincipalName = authentication.getName();
		
		String username = getClientIpAddress(request);
		if (authentication != null) {
			if(!(authentication instanceof AnonymousAuthenticationToken)) {
				
				username = authentication.getName();
			}
		}
		
		deed.setCreator(username);

		return deedRepository.save(deed);
	}

	@PostMapping("/poc")
	public PocCase createPocCase(@Valid @RequestBody PocCase pocCase, HttpServletRequest request) {
		String remoteAddr = request.getRemoteAddr();
		pocCase.setIpAddress(remoteAddr);
		
		return pocCaseService.createPocCase(pocCase);
	}

	@GetMapping("/poc/{deedId}")
	public void visitByDeed(@PathVariable("deedId") long deedId, HttpServletRequest request,
			HttpServletResponse response) throws IOException {

		String remoteAddr = getClientIpAddress(request);
		PocCase pocCase = PocCase.builder().ipAddress(remoteAddr).deedId(deedId).userName(remoteAddr).build();

		pocCaseService.createPocCase(pocCase);

		response.sendRedirect("/");
	}

	@GetMapping("/poc/impact/{deedId}")
	public List<PocCase> getImpactNumberByDeedId(@PathVariable("deedId") long deedId, HttpServletRequest request,
			HttpServletResponse response) {

		String remoteAddr = getClientIpAddress(request);

		return pocCaseService.findImpactByDeedIdAndUserName(deedId, remoteAddr);
	}
	
	@GetMapping("/poc/impact")
	public List<PocCase> getImpactNumber(HttpServletRequest request,
			HttpServletResponse response, Authentication authentication) {
		
		String username = null;
		if (authentication != null) {
			if(!(authentication instanceof AnonymousAuthenticationToken)) {
				
				username = authentication.getName();
			}
		} else {
			username = getClientIpAddress(request);
		}

		

		return pocCaseService.findImpactByUserName(username);
	}

	public static String getClientIpAddress(HttpServletRequest request) {
		String xForwardedForHeader = request.getHeader("X-Forwarded-For");
		if (xForwardedForHeader == null) {
			return request.getRemoteAddr();
		} else {
			// As of https://en.wikipedia.org/wiki/X-Forwarded-For
			// The general format of the field is: X-Forwarded-For: client, proxy1, proxy2
			// ...
			// we only want the client
			return new StringTokenizer(xForwardedForHeader, ",").nextToken().trim();
		}
	}
}

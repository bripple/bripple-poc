package io.bripple.poc.repository;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import io.bripple.poc.model.PocCase;

@RepositoryRestResource(exported=true, path="pocCases")
public interface PocCaseRepository extends PagingAndSortingRepository<PocCase, Long> {
	
	  List<PocCase> findByDeedIdAndUserNameOrderByUpdateTimeAsc(long deedId, String userName);
	  
	  List<PocCase> findByUserNameOrderByUpdateTimeAsc(String userName);
	  
	  List<PocCase> findByDeedIdAndUpdateTimeGreaterThanEqual(Long deedId, LocalDateTime updateTime);
	  
	  List<PocCase> findByDeedCreatorOrderByUpdateTimeAsc(String deedCreator);
	  
	  
//	  List<PocCase> findByUpdateTimeGreaterThanEqual(Long deedId, LocalDateTime updateTime);
}
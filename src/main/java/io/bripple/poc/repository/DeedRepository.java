package io.bripple.poc.repository;

import org.springframework.data.repository.CrudRepository;
import io.bripple.poc.model.Deed;


public interface DeedRepository extends CrudRepository<Deed, Long> {

}

# Bripple Web Service

## Working Directory
bripple-poc

## Build Command
```
./mvnw clean package

To only build backend without front-end
./mnvw clean package -P backend

```

## Start Web Service

### Setup Environment Variables
Springboot's src/main/resources/applications.properties uses environment variable for DB settings. It is because Heruko uses env variable to pass in the DB settings.

On the local env, please run "source script/setEnv.sh" to set the env variable at local machine

### Postgres Docker Instance
Create a stack.yml then run "docker-compose -f stack.yml up"
```
#Use postgres/example user/password credentials
version: '3.1'
services:
  db:
    image: postgres
    restart: always
    environment:
      POSTGRES_PASSWORD: example
  adminer:
    image: adminer
    restart: always
    ports:
      - 8090:8080
```
A postgre admin web tool will be running on
http://localhost:8090

[For more detail] https://hub.docker.com/_/postgres/
      
## Start Web Service
###Development mode (Using H2 Memeory DB)

```
./mvnw spring-boot:run -Dspring-boot.run.profiles=dev

```

###Paraell run with backend only and ev profile
mvn -T 4 spring-boot:run -P backend -Dspring-boot.run.profiles=dev


###Production
```
./mvnw spring-boot:run
```


### Frontend Development
```
cd ./src/main/js
npm start
```
You will see the following console output
```
Compiled successfully!

You can now view spring-data-rest-and-reactjs in the browser.

  Local:            http://localhost:3000/
  On Your Network:  http://10.0.0.12:3000/
  HTTPS: 			https://localhost:8443


Note that the development build is not optimized.
To create a production build, use npm run build.
```
Change a js file and you will see the output on web page immediately without browser refresh.

### Frontend Unit Test for Development
On terminal, enter following command to enter jest test mode.
```
./ui_test.sh
```
You shall see the follow output
```
Test Suites: 1 skipped, 1 passed, 1 of 2 total
Tests:       1 skipped, 2 passed, 3 total
Snapshots:   0 total
Time:        43.577s
Ran all test suites with tests matching ".*Print.*".

Watch Usage: Press w to show more.
```
Press w and select t to filter tests that you want to run during development.
For example: ".*Print.*" or "describe test name" like "Test Print feature Print type selection"




